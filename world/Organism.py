from world.Config import Config


class Organism:
    def __init__(self, world, x, y):
        self.__power = 0
        self.__initiative = 0
        self.__x_position = x
        self.__y_position = y
        self.__age = 0
        self.__indicator = Config.default_organism_color
        self.__world = world
        self.__dead_log = ""

    def action(self):
        pass

    def collision(self, oponent):
        pass

    def multiply(self, parent1, parent2):
        pass

    def born_me(self, world, x, y):
        pass

    def get_x_position(self):
        return self.__x_position

    def set_x_position(self, x):
        self.__x_position = x

    def get_y_position(self):
        return self.__y_position

    def set_y_position(self, y):
        self.__y_position = y

    def get_power(self):
        return self.__power

    def get_indicator(self):
        return self.__indicator

    def set_indicator(self, indicator):
        self.__indicator = indicator

    def set_power(self, power):
        self.__power = power

    def get_age(self):
        return self.__age

    def set_age(self, age):
        self.__age = age

    def get_initiative(self):
        return self.__initiative

    def set_initiative(self, initiative):
        self.__initiative = initiative

    def get_world(self):
        return self.__world

    def set_world(self, world):
        self.__world = world

    def set_dead_log(self, log):
        self.__dead_log = log

    def get_dead_log(self):
        return self.__dead_log

    @staticmethod
    def in_boundaries(x, y):
        size = 20
        if x >= 0 and x < size and y >= 0 and y < size:
            return True
        return False

    def is_empty_place(self, organism_x, organism_y):
        if self.in_boundaries(organism_x, organism_y) is False:
            return False
        elif self.get_world().container.get_organism_by_xy(organism_x, organism_y) is None:
            return True
        return False

    @staticmethod
    def check_boundaries(n):
        size = 20
        for i in range(2):
            if n >= size:
                n -= 1

        for i in range(2):
            if n < 0:
                n += 1
        return n
