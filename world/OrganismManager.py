class OrganismManager:
    def __init__(self):
        self.__organisms = []

    def get_organism_by_xy(self, x, y):
        for organism in self.__organisms:
            if organism.get_x_position() == x and organism.get_y_position() == y:
                return organism
        return None

    def get_size(self):
        return len(self.__organisms)

    def get_organisms(self):
        return self.__organisms

    def set_organisms(self, new_organisms):
        self.__organisms = new_organisms

    def delete_organism(self, organism):
        try:
            self.__organisms.remove(organism)
            organism.get_world().add_log(organism.get_dead_log())
            # print(organism.get_dead_log())
        except ValueError:
            pass
