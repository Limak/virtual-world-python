class Config:
    # kolory
    default_organism_color = '#BDFFC1'
    antelope_color = '#FFDB5E'
    fox_color = '#FE5D04'
    human_color = '#8CA2A6'
    sheep_color = '#FAFFFC'
    turtle_color = '#3C52FF'
    wolf_color = '#000000'
    grass_color = '#28FF1A'
    guarana_color = '#FF2552'
    milk_flower_color = '#FFFE04'
    wolf_berries_color = '#C111C2'
    sosnowski_barszcz_color = 'red'
    cyber_sheep_color = 'cyan'

    # prawdopodobienstwa dla organizmow
    probability_for_plants = 4
    rand_max_distance = 3
    true_for_plnats_multiplication = 2
    antelope_max_distance = 5
    probability_for_turtle = 4
    true_for_turtle_action = 2

    # logi
    antelope_log = "Antylopa zginela\n"
    fox_log = "Lis zginal\n"
    human_log = "Czlowiek zostal zabity\n"
    sheep_log = "Owca zginela\n"
    turtle_log = "Zolw zginal\n"
    wolf_log = "Wilk zginal\n"
    cyber_sheep_log = "Cyber owca zginela\n"

    grass_log = "Trawa zostala zjedzona\n"
    guarana_log = "guarana zostala zjedzona\n"
    milk_flower_log = "mlecz zostal zjedzony\n"
    sosnowski_barszcz_log = "Barscz zostal zjedzony\n"
    wolf_berries_log = "Jagody zostaly zjedzone\n"

    # okienko gry
    author = "Kamil Mastalerz 165296"
    window_height = 500
    window_width = 600
