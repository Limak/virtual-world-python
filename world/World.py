import pickle
from operator import attrgetter

from OrganismManager import OrganismManager
from animals.Antelope import Antelope
from animals.CyberSheep import CyberSheep
from animals.Fox import Fox
from animals.Human import Human
from animals.Sheep import Sheep
from animals.Turtle import Turtle
from animals.Wolf import Wolf
from plants.Grass import Grass
from plants.Guarana import Guarana
from plants.MilkFlower import MilkFlower
from plants.SosnowskiBarszcz import SosnowskiBarszcz
from plants.WolfBerries import WolfBerries


class World:
    def __init__(self):
        self.container = OrganismManager()
        self.__map = [['#BDFFC1'] * 20 for i in range(20)]
        self.__round_counter = 0
        self.initialise()
        self.fill_map()
        self.__logs = ""

    def next_round(self):
        self.container.set_organisms(
            sorted(self.container.get_organisms(), key=attrgetter('_Organism__initiative', '_Organism__age'),
                   reverse=True))

        if self.find_human() is not None:
            self.find_human().check_using_super_power()

        size = self.container.get_size()

        if size >= 400:
            pass

        for j in range(size):
            if j >= size:
                break

            current = self.container.get_organisms()[j]
            current.set_age(current.get_age() + 1)
            current.action()

            if size > self.container.get_size():
                size = self.container.get_size()

        self.__round_counter += 1
        self.fill_map()
        # del self.container.get_logs()[:]

    def initialise(self):
        self.container.get_organisms().append(Antelope(self, 10, 10))
        self.container.get_organisms().append(Antelope(self, 19, 10))
        self.container.get_organisms().append(Fox(self, 10, 12))
        self.container.get_organisms().append(Fox(self, 17, 13))
        self.container.get_organisms().append(Wolf(self, 15, 10))
        self.container.get_organisms().append(Wolf(self, 3, 11))
        self.container.get_organisms().append(Turtle(self, 13, 12))
        self.container.get_organisms().append(Turtle(self, 19, 13))
        self.container.get_organisms().append(CyberSheep(self, 18, 18))
        self.container.get_organisms().append(Sheep(self, 11, 19))
        self.container.get_organisms().append(Sheep(self, 17, 3))
        self.container.get_organisms().append(Guarana(self, 1, 5))
        self.container.get_organisms().append(Guarana(self, 5, 7))
        self.container.get_organisms().append(SosnowskiBarszcz(self, 18, 1))
        self.container.get_organisms().append(MilkFlower(self, 6, 15))
        self.container.get_organisms().append(Grass(self, 8, 11))
        self.container.get_organisms().append(Grass(self, 10, 2))
        self.container.get_organisms().append(WolfBerries(self, 4, 15))
        self.container.get_organisms().append(WolfBerries(self, 17, 9))
        self.container.get_organisms().append(Human(self, 1, 4))

    def fill_map(self):
        self.clear_map()
        for organism in self.container.get_organisms():
            x = organism.get_x_position()
            y = organism.get_y_position()

            if x < 20 and y < 20:
                self.__map[y][x] = organism.get_indicator()

    def clear_map(self):
        self.__map = [['#BDFFC1'] * 20 for k in range(20)]

    def get_round_counter(self):
        return self.__round_counter

    def get_map(self):
        return self.__map

    def find_human(self):

        for organism in self.container.get_organisms():
            if organism is not None and isinstance(organism, Human):
                return organism

        return None

    def get_container(self):
        return self.container

    def save_game(self):
        pickle.dump(self.__round_counter, open("roundCounter.p", "wb"))
        pickle.dump(self.container, open("save.p", "wb"))

    def load_game(self):
        self.__round_counter = pickle.load(open("roundCounter.p"))
        self.container = pickle.load(open("save.p"))

        for organism in self.container.get_organisms():
            organism.set_world(self)

        self.fill_map()

    def get_logs(self):
        return self.__logs

    def add_log(self, log):
        self.__logs += log

    def clear_logs(self):
        self.__logs = ""
