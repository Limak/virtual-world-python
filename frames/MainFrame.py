import warnings

# from gi.repository import gtk
import gtk.gdk

from frames.AddingFrame import AddingFrame
from world.Config import Config


class MyFrame(gtk.Window):
    def __init__(self, world):
        super(MyFrame, self).__init__()
        self.connect("key-press-event", self.on_window_key_press_event)
        self.connect("destroy", gtk.main_quit)
        self.set_size_request(Config.windo_width, Config.window_height)
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_title(Config.author)
        self.set_resizable(False)
        self.frameworld = world

        view_type = gtk.Fixed()

        self.table = gtk.TreeView(self.create_map())
        self.table.set_headers_visible(False)
        self.create_columns(self.table)
        self.table.get_selection().set_mode(gtk.SELECTION_NONE)
        self.table.add_events(gtk.gdk.BUTTON_PRESS_MASK)
        self.table.connect('button-press-event', self.on_click)
        self.table.set_fixed_height_mode(True)

        self.roundCounterLabel = gtk.Label("Runda: " + str(self.frameworld.get_round_counter()))
        self.power_label = gtk.Label("Moc czlowieka: " + str(self.frameworld.find_human().get_power()))
        self.time_label = gtk.Label("Czas do supermocy: " +
                                    str(self.frameworld.find_human().get_super_power_wait_counter()))

        self.logs_label = gtk.Label("Logi:\n" + self.frameworld.get_logs())
        self.frameworld.clear_logs()

        new_round_button = gtk.Button("Nowa runda")
        new_round_button.set_size_request(100, 40)
        new_round_button.connect("clicked", self.on_new_round_button_clicked)

        save_button = gtk.Button("Zapisz gre")
        save_button.set_size_request(100, 40)
        save_button.connect("clicked", self.on_save_button_clicked)

        open_button = gtk.Button("Otworz gre")
        open_button.set_size_request(100, 40)
        open_button.connect("clicked", self.on_open_button_clicked)

        super_power_button = gtk.Button("Supermoc")
        super_power_button.set_size_request(100, 40)
        super_power_button.connect("clicked", self.on_super_power_button_clicked)

        view_type.put(new_round_button, 420, 100)
        view_type.put(save_button, 420, 150)
        view_type.put(open_button, 420, 200)
        view_type.put(super_power_button, 420, 250)
        view_type.put(self.roundCounterLabel, 420, 300)
        view_type.put(self.logs_label, 420, 352)
        view_type.put(self.power_label, 420, 10)
        view_type.put(self.time_label, 420, 35)
        view_type.put(self.table, 0, 0)

        self.add(view_type)
        self.show_all()

    def create_map(self):
        map_model = gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str,
                                  str, str, str)
        for i in range(20):
            map_model.append(self.frameworld.get_map()[i])
        return map_model

    @staticmethod
    def create_columns(tree_view):
        for i in range(20):
            cell = gtk.CellRendererText()
            col = gtk.TreeViewColumn("", cell, background=i)
            col.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
            col.set_fixed_width(20)
            tree_view.append_column(col)

    def on_new_round_button_clicked(self, button):
        self.frameworld.next_round()
        self.roundCounterLabel.set_text("Runda: " + str(self.frameworld.get_round_counter()))
        human = self.frameworld.find_human()
        if human is not None:
            self.power_label.set_text("Moc czlowieka: " + str(self.frameworld.find_human().get_power()))
            self.time_label.set_text("czas do supermocy: " +
                                     str(self.frameworld.find_human().get_super_power_wait_counter()))
        self.logs_label.set_text("Logi:\n" + str(self.frameworld.get_logs()))
        self.frameworld.clear_logs()
        self.table.set_model(self.create_map())

    def on_save_button_clicked(self, button):
        self.frameworld.save_game()
        self.table.set_model(self.create_map())

    def on_open_button_clicked(self, button):
        self.frameworld.load_game()
        self.roundCounterLabel.set_text("Runda: " + str(self.frameworld.get_round_counter()))
        human = self.frameworld.find_human()
        if human is not None:
            self.power_label.set_text("Moc czlowieka: " + str(self.frameworld.find_human().get_power()))
            self.time_label.set_text("czas do supermocy: " +
                                     str(self.frameworld.find_human().get_super_power_wait_counter()))
        self.logs_label.set_text("Logi:\n" + str(self.frameworld.get_logs()))
        self.frameworld.clear_logs()
        self.table.set_model(self.create_map())

    def on_super_power_button_clicked(self, button):
        human = self.frameworld.find_human()

        if human is not None:
            if human.get_super_power_wait_counter() == 0:
                if not human.is_should_working_timer():
                    human.set_additional_super_power(5)
                    human.set_tmp_power(human.get_power())
                    human.set_power(human.get_power() + human.get_additional_super_power())
                    human.set_should_working_timer(True)

    def on_window_key_press_event(self, window, event):
        key = gtk.gdk.keyval_name(event.keyval)
        human = self.frameworld.find_human()

        if human is not None:
            if key == "Up":
                human.set_next_x(0)
                human.set_next_y(-1)
            elif key == "Down":
                human.set_next_x(0)
                human.set_next_y(1)
            elif key == "Left":
                human.set_next_x(-1)
                human.set_next_y(0)
            elif key == "Right":
                human.set_next_x(1)
                human.set_next_y(0)

        self.frameworld.next_round()
        self.roundCounterLabel.set_text("Runda: " + str(self.frameworld.get_round_counter()))
        self.logs_label.set_text("Logi:\n" + str(self.frameworld.get_logs()))
        print (self.frameworld.get_logs())
        self.frameworld.clear_logs()
        human = self.frameworld.find_human()
        if human is not None:
            self.power_label.set_text("Moc czlowieka: " + str(self.frameworld.find_human().get_power()))
            self.time_label.set_text("czas do supermocy: " +
                                     str(self.frameworld.find_human().get_super_power_wait_counter()))
        self.table.set_model(self.create_map())

    def on_click(self, widget, event):
        self.adding_frame = AddingFrame(self)
        self.adding_frame.set_xy((int(event.x) / 20), ((int(event.y)) / 23))  # dzielone przez rozmiary kratki
        self.adding_frame.show_all()

        warnings.simplefilter("ignore")
        self.adding_frame.set_focus(self.adding_frame)
