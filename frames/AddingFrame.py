import gtk

from animals.Antelope import Antelope
from animals.CyberSheep import CyberSheep
from animals.Fox import Fox
from animals.Sheep import Sheep
from animals.Turtle import Turtle
from animals.Wolf import Wolf
from plants.Grass import Grass
from plants.Guarana import Guarana
from plants.MilkFlower import MilkFlower
from plants.SosnowskiBarszcz import SosnowskiBarszcz
from plants.WolfBerries import WolfBerries


class AddingFrame(gtk.Window):
    def __init__(self, frame):
        super(AddingFrame, self).__init__()
        self.set_size_request(900, 80)
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_title("Kamil Mastalerz 165296")
        self.set_resizable(False)
        self.parentFrame = frame
        self.col = 0
        self.row = 0

        antelope = gtk.Button("Antylopa")
        antelope.set_size_request(100, 40)
        antelope.connect("clicked", self.antelope_clicked)

        fox = gtk.Button("Lis")
        fox.set_size_request(100, 40)
        fox.connect("clicked", self.fox_clicked)

        sheep = gtk.Button("Owca")
        sheep.set_size_request(100, 40)
        sheep.connect("clicked", self.sheep_clicked)

        tortoise = gtk.Button("Zolw")
        tortoise.set_size_request(100, 40)
        tortoise.connect("clicked", self.turtle_clicked)

        wolf = gtk.Button("Wilk")
        wolf.set_size_request(100, 40)
        wolf.connect("clicked", self.wolf_clicked)

        berry = gtk.Button("Jagoda")
        berry.set_size_request(100, 40)
        berry.connect("clicked", self.wolf_berries_clicked)

        dandelion = gtk.Button("Mlecz")
        dandelion.set_size_request(100, 40)
        dandelion.connect("clicked", self.milk_flower_clicked)

        grass = gtk.Button("Trawa")
        grass.set_size_request(100, 40)
        grass.connect("clicked", self.grass_clicked)

        guarana = gtk.Button("Guarana")
        guarana.set_size_request(100, 40)
        guarana.connect("clicked", self.guarana_clicked)

        sosnowski_barszcz = gtk.Button("Barszcz")
        sosnowski_barszcz.set_size_request(100, 40)
        sosnowski_barszcz.connect("clicked", self.sosnowski_barszcz_clicked)

        cyber_sheep = gtk.Button("Cyber Owca")
        cyber_sheep.set_size_request(100, 40)
        cyber_sheep.connect("clicked", self.cyber_sheep_clicked)

        view_type = gtk.Fixed()
        view_type.put(antelope, 0, 0)
        view_type.put(fox, 100, 0)
        view_type.put(sheep, 200, 0)
        view_type.put(tortoise, 300, 0)
        view_type.put(wolf, 400, 0)
        view_type.put(berry, 500, 0)
        view_type.put(dandelion, 600, 0)
        view_type.put(grass, 700, 0)
        view_type.put(guarana, 800, 0)
        view_type.put(sosnowski_barszcz, 0, 40)
        view_type.put(cyber_sheep, 100, 40)

        self.add(view_type)

    def antelope_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            Antelope(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def fox_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            Fox(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def sheep_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            Sheep(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def turtle_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            Turtle(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def wolf_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            Wolf(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def wolf_berries_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            WolfBerries(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def milk_flower_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            MilkFlower(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def grass_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            Grass(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def guarana_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            Guarana(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def sosnowski_barszcz_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            SosnowskiBarszcz(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def cyber_sheep_clicked(self, button):
        self.parentFrame.frameworld.container.get_organisms().append(
            CyberSheep(self.parentFrame.frameworld, self.row, self.col))
        self.parentFrame.frameworld.fill_map()
        self.parentFrame.table.set_model(self.parentFrame.create_map())
        self.destroy()

    def set_xy(self, x, y):
        self.col = y
        self.row = x
