import random

from animals.Animal import Animal
from world.Config import Config


class Turtle(Animal):
    def __init__(self, world, x, y):
        Animal.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(2)
        self.set_initiative(1)
        self.set_indicator(Config.turtle_color)

    def action(self):
        if random.randrange(Config.probability_for_turtle) == Config.true_for_turtle_action:
            Animal.action(self)

    def born_me(self, world, x, y):
        return Turtle(world, x, y)
