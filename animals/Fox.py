import random

from animals.Animal import Animal
from world.Config import Config


class Fox(Animal):
    def __init__(self, world, x, y):
        Animal.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(3)
        self.set_initiative(7)
        self.set_indicator(Config.fox_color)

    def action(self):
        x = random.randrange(Config.rand_max_distance) - 1
        y = random.randrange(Config.rand_max_distance) - 1

        x = self.check_boundaries(x + self.get_x_position())
        y = self.check_boundaries(y + self.get_y_position())

        oponent = self.get_world().container.get_organism_by_xy(x, y)

        if oponent is None:
            self.set_x_position(x)
            self.set_y_position(y)
        elif oponent.get_power() < self.get_power() or oponent.get_indicator() == self.get_indicator():
            oponent.collision(self)

    def born_me(self, world, x, y):
        return Fox(world, x, y)
