from animals.Animal import Animal
from world.Config import Config


class Sheep(Animal):
    def __init__(self, world, x, y):
        Animal.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(4)
        self.set_initiative(4)
        self.set_indicator(Config.sheep_color)
        self.set_dead_log(Config.sheep_log)

    def born_me(self, world, x, y):
        return Sheep(world, x, y)
