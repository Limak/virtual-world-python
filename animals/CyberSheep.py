from animals.Animal import Animal
from plants.SosnowskiBarszcz import SosnowskiBarszcz
from world.Config import Config


class CyberSheep(Animal):
    __minDistance = 0
    __targets_amount = 0

    def __init__(self, world, x, y):
        Animal.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(11)
        self.set_initiative(4)
        self.set_indicator(Config.cyber_sheep_color)
        self.set_dead_log(Config.cyber_sheep_log)

    def action(self):
        self.callculate_barszcz_amount()
        if self.__targets_amount == 0:
            self.default_action()
        elif self.__targets_amount == 1:
            self.find_first_distance()
            self.do_next_step()
        else:
            self.find_first_distance()
            self.find_min_distance()
            self.do_next_step()

    def collision(self, oponent):
        if isinstance(oponent, SosnowskiBarszcz):
            self.get_world().container.get_organisms().delete_organism(oponent)
        else:
            self.default_collision(oponent)

    # Uzywane tylko wtedy gdy jest wiecej niz 1 Barszcz
    def find_min_distance(self):
        for organism in self.get_world().container.get_organisms():
            tmp_distance = self.find_distance(self, organism)
            if isinstance(organism, SosnowskiBarszcz) and self.__minDistance > tmp_distance:
                self.__minDistance = tmp_distance

    def find_first_distance(self):
        for organism in self.get_world().container.get_organisms():
            if isinstance(organism, SosnowskiBarszcz):
                first_distance = self.find_distance(self, organism)
                self.__minDistance = first_distance
                return

    def callculate_barszcz_amount(self):
        barszcz_amount = 0
        for organism in self.get_world().container.get_organisms():
            if isinstance(organism, SosnowskiBarszcz):
                barszcz_amount += 1
        self.__targets_amount = barszcz_amount

    def do_next_step(self):
        for organism in self.get_world().container.get_organisms():
            tmp_distance = self.find_distance(self, organism)
            if isinstance(organism, SosnowskiBarszcz) and tmp_distance == self.__minDistance:
                x1 = self.get_x_position()
                y1 = self.get_y_position()
                x2 = organism.get_x_position()
                y2 = organism.get_y_position()

                tmp_x = 0
                tmp_y = 0
                if x1 != x2 or y1 != y2:
                    if x1 - x2 > 0:
                        # self.set_x_position(x1 - 1)
                        tmp_x = x1 - 1
                    elif x1 - x2 < 0:
                        # self.set_x_position(x1 + 1)
                        tmp_x = x1 + 1
                    else:
                        tmp_x = x1

                    if y1 - y2 > 0:
                        # self.set_y_position(y1 - 1)
                        tmp_y = y1 - 1
                    elif y1 - y2 < 0:
                        # self.set_y_position(y1 + 1)
                        tmp_y = y1 + 1
                    else:
                        tmp_y = y1
                    self.manage_collision(tmp_x, tmp_y)
                else:
                    break

    @staticmethod
    def find_distance(org1, org2):
        x1 = org1.get_x_position()
        y1 = org1.get_y_position()
        x2 = org2.get_x_position()
        y2 = org2.get_y_position()
        distance = 0

        while x1 != x2 or y1 != y2:
            if x1 - x2 > 0:
                x1 -= 1
            elif x1 - x2 < 0:
                x1 += 1

            if y1 - y2 > 0:
                y1 -= 1
            elif y1 - y2 < 0:
                y1 += 1
            distance += 1
        return distance
