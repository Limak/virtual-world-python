import random

from animals.Animal import Animal
from world.Config import Config


class Antelope(Animal):
    def __init__(self, world, x, y):
        Animal.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_world(world)
        self.set_initiative(4)
        self.set_power(4)
        self.set_indicator(Config.antelope_color)
        self.set_dead_log(Config.antelope_log)

    def action(self):
        x = random.randrange(Config.antelope_max_distance) - 2
        y = random.randrange(Config.antelope_max_distance) - 2

        x = self.check_boundaries(x + self.get_x_position())
        y = self.check_boundaries(y + self.get_y_position())

        oponent = self.get_world().container.get_organism_by_xy(x, y)

        if oponent is None:
            self.set_x_position(x)
            self.set_y_position(y)
        elif oponent != self:
            oponent.collision(self)

    def collision(self, oponent):
        if oponent.get_indicator() == self.get_indicator():
            self.multiply(self, oponent)

        if random.randrange(100) % 2 == 0:
            x = self.get_x_position()
            y = self.get_y_position()

            while x == self.get_x_position() and y == self.get_y_position():
                self.action()

        elif oponent.get_power() < self.get_power():
            self.get_world().container.get_organisms().remove(oponent)
        else:
            oponent.set_x_position(self.get_x_position())
            oponent.set_y_position(self.get_y_position())
            oponent.get_world().container.get_organisms().remove(self)
