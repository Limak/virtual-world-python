from animals.Animal import Animal
from world.Config import Config


class Human(Animal):
    __tmp_power = 0
    __additional_super_power = 0
    __super_power_wait_counter = 0

    def __init__(self, world, x, y):
        Animal.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(5)
        self.set_initiative(4)
        self.set_indicator(Config.human_color)
        self.set_dead_log(Config.human_log)
        self.__next_x = 0
        self.__next_y = 0
        # pola do super mocy
        self.__should_working_timer = False

    def action(self):
        x = self.get_x_position() + self.__next_x
        y = self.get_y_position() + self.__next_y

        oponent = self.get_world().container.get_organism_by_xy(x, y)

        if oponent is None:
            self.set_x_position(x)
            self.set_y_position(y)
        else:
            oponent.collision(self)

    def collision(self, oponent):
        if self == oponent:
            pass

        if oponent.get_indicator() == self.get_indicator():
            self.multiply(self, oponent)
        elif oponent.get_power() < self.get_power():
            self.get_world().container.delete_organism(oponent)
        else:
            oponent.set_x_position(self.get_x_position())
            oponent.set_y_position(self.get_y_position())
            oponent.get_world().container.delete_organism(self)

    def born_me(self, world, x, y):
        return None

    def check_using_super_power(self):
        if self.__additional_super_power > 0:
            self.__additional_super_power -= 1
            self.set_power(self.get_power() - 1)

        elif self.__super_power_wait_counter == 0 and self.get_power() == self.__tmp_power:
            if self.__should_working_timer:
                self.set_super_power_wait_counter(5)
                self.__should_working_timer = False
        elif self.__super_power_wait_counter > 0:
            self.__super_power_wait_counter -= 1
        else:
            self.__super_power_wait_counter = 0
            self.__should_working_timer = False

    def is_should_working_timer(self):
        return self.__should_working_timer

    def set_should_working_timer(self, should_working_timer):
        self.__should_working_timer = should_working_timer

    def get_additional_super_power(self):
        return self.__additional_super_power

    def set_additional_super_power(self, additional_super_power):
        self.__additional_super_power = additional_super_power

    def get_tmp_power(self):
        return self.__tmp_power

    def get_should_working_timer(self):
        return self.__should_working_timer

    def set_tmp_power(self, tmp_power):
        self.__tmp_power = tmp_power

    def get_super_power_wait_counter(self):
        return self.__super_power_wait_counter

    def set_super_power_wait_counter(self, super_power_wait_counter):
        self.__super_power_wait_counter = super_power_wait_counter

    def set_next_x(self, x):
        self.__next_x = x

    def set_next_y(self, y):
        self.__next_y = y
