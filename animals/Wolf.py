from animals.Animal import Animal
from world.Config import Config


class Wolf(Animal):
    def __init__(self, wolf, x, y):
        Animal.__init__(self, wolf, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(2)
        self.set_initiative(1)
        self.set_indicator(Config.wolf_color)
        self.set_dead_log(Config.wolf_log)

    def born_me(self, world, x, y):
        return Wolf(world, x, y)
