import random

from world.Organism import Organism


class Animal(Organism):
    def __init__(self, world, x, y):
        Organism.__init__(self, world, x, y)

    def action(self):
        self.default_action()

    def collision(self, oponent):
        self.default_collision(oponent)

    def default_collision(self, oponent):
        if self == oponent:
            pass

        if oponent.get_indicator() == self.get_indicator():
            self.multiply(self, oponent)
        elif oponent.get_power() < self.get_power():
            self.get_world().container.delete_organism(oponent)
        else:
            oponent.set_x_position(self.get_x_position())
            oponent.set_y_position(self.get_y_position())
            oponent.get_world().container.delete_organism(self)

    def default_action(self):
        x = random.randrange(3) - 1
        y = random.randrange(3) - 1

        x = self.check_boundaries(x + self.get_x_position())
        y = self.check_boundaries(y + self.get_y_position())

        self.manage_collision(x, y)

        # oponent = self.get_world().container.get_organism_by_xy(x, y)
        #
        # if oponent is None:
        #     self.set_x_position(x)
        #     self.set_y_position(y)
        # elif oponent != self:
        #     oponent.collision(self)

    def manage_collision(self, x, y):
        oponent = self.get_world().container.get_organism_by_xy(x, y)

        if oponent is None:
            self.set_x_position(x)
            self.set_y_position(y)
        elif oponent != self:
            oponent.collision(self)

    def multiply(self, parent1, parent2):
        if parent1 == parent2:
            return

        for x in range(-2, 2):
            for y in range(-1, 2):
                x = self.check_boundaries(x + parent1.get_x_position())
                y = self.check_boundaries(y + parent1.get_y_position())

                oponent = self.get_world().container.get_organism_by_xy(x, y)

                if oponent is None or (oponent.get_power() < parent1.get_power()):
                    parent1.get_world().container.get_organisms().append(parent1.born_me(parent1.get_world(), x, y))
                    break
                elif oponent == parent2:
                    continue
