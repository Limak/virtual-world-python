from animals.Animal import Animal
from plants.Plant import Plant
from world.Config import Config


class Guarana(Plant):
    def __init__(self, world, x, y):
        Plant.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(0)
        self.set_initiative(0)
        self.set_indicator(Config.guarana_color)
        self.set_dead_log(Config.guarana_log)

    def collision(self, oponent):
        if (isinstance(oponent, Animal)):
            oponent.set_power(oponent.get_power() + 3)
            oponent.set_x_position(self.get_x_position())
            oponent.set_y_position(self.get_y_position())
            oponent.get_world().container.delete_organism(self)

    def born_me(self, world, x, y):
        return Guarana(world, x, y)
