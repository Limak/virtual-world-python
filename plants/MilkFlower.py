from plants.Plant import Plant
from world.Config import Config


class MilkFlower(Plant):
    def __init__(self, world, x, y):
        Plant.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(0)
        self.set_initiative(0)
        self.set_indicator(Config.milk_flower_color)
        self.set_dead_log(Config.milk_flower_log)

    def action(self):
        for i in range(3):
            Plant.action(self)

    def born_me(self, world, x, y):
        return MilkFlower(world, x, y)
