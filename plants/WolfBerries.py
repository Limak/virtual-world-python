from animals.Animal import Animal
from plants.Plant import Plant
from world.Config import Config


class WolfBerries(Plant):
    def __init__(self, world, x, y):
        Plant.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_initiative(0)
        self.set_power(99)
        self.set_indicator(Config.wolf_berries_color)
        self.set_dead_log(Config.wolf_berries_log)

    def collision(self, oponent):
        if oponent == self:
            pass
        elif isinstance(oponent, Animal):
            self.get_world().container.delete_organism(oponent)
            # self.get_world().container.delete_organism(self)

    def born_me(self, world, x, y):
        return WolfBerries(world, x, y)
