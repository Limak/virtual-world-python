from plants.Plant import Plant
from world.Config import Config


class Grass(Plant):
    def __init__(self, world, x, y):
        Plant.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_initiative(0)
        self.set_power(0)
        self.set_indicator(Config.grass_color)
        self.set_dead_log(Config.grass_log)

    def born_me(self, world, x, y):
        return Grass(world, x, y)
