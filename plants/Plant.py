import random

from world.Config import Config
from world.Organism import Organism


class Plant(Organism):
    def __init__(self, world, x, y):
        Organism.__init__(self, world, x, y)

    def action(self):
        temp = random.randrange(Config.probability_for_plants)

        if temp == Config.true_for_plnats_multiplication:
            # losowanie kierunku
            x = random.randrange(Config.rand_max_distance) - 1
            y = random.randrange(Config.rand_max_distance) - 1

            x = self.check_boundaries(x + self.get_x_position())
            y = self.check_boundaries(y + self.get_y_position())

            oponent = self.get_world().container.get_organism_by_xy(x, y)

            if oponent is None:
                newone = self.born_me(self.get_world(), x, y)
                self.get_world().container.get_organisms().append(newone)

    def collision(self, oponent):
        oponent.set_x_position(self.get_x_position())
        oponent.set_y_position(self.get_y_position())
        self.get_world().container.delete_organism(self)

    def born_me(self, world, x, y):
        return None
