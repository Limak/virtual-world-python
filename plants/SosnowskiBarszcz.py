from animals.Animal import Animal
from plants.Plant import Plant
from world.Config import Config


class SosnowskiBarszcz(Plant):
    def __init__(self, world, x, y):
        Plant.__init__(self, world, x, y)
        self.set_x_position(x)
        self.set_y_position(y)
        self.set_power(10)
        self.set_initiative(0)
        self.set_indicator(Config.sosnowski_barszcz_color)
        self.set_dead_log(Config.sosnowski_barszcz_log)

    def collision(self, oponent):
        if oponent.get_indicator() != Config.cyber_sheep_color:
            self.get_world().container.delete_organism(oponent)
        else:
            self.get_world().container.delete_organism(self)

    def action(self):

        for x in range(-1, 2):
            for y in range(-1, 2):
                compared_organism = self.get_world().container.get_organism_by_xy(x, y)

                if compared_organism is not None:
                    if self.in_boundaries(compared_organism.get_x_position() + x,
                                          compared_organism.get_y_position() + y):
                        if not self.is_empty_place(compared_organism.get_x_position() + x,
                                                   compared_organism.get_y_position() + y):
                            if isinstance(compared_organism, Animal):
                                self.get_world().container.delete_organism(compared_organism)
                            else:
                                continue

    def born_me(self, world, x, y):
        return SosnowskiBarszcz(world, x, y)
